package Machine;

import jade.core.Agent;

public class Frodon extends Machine {

    private Agent receveur ;
    private Agent mobile =null;
    private boolean etat =false;

    public Frodon(){
        do{
        do
        {
            System.out.println();        }
        while(etat);
        
        } while(true);

    }

    public void envoyerMobile(Intermediaire machine){
        machine.setMobile(mobile);
        machine.setEtat(true);
        setMobile(null);
        setEtat(false);

    }

    public Agent getReceveur() {
        return receveur;
    }

    public void setReceveur(Agent receveur) {
        this.receveur = receveur;
    }

    public Agent getMobile() {
        return mobile;
    }

    public void setMobile(Agent mobile) {
        this.mobile = mobile;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }
}

package Machine;


import Compteur.Compteur;
import jade.core.Agent;

public class Intermediaire extends Machine{
    // Instance de l'agent Compteur qui va etre contenu dans chaque machine
    private Compteur compteur =new Compteur();
    
    // Agent aguille 
    private Agent aiguille = new Agent();
    // mobile va nous servir a stocker l'agent mobile ,apres l'avoir d'une machine voisine
    private Agent mobile =null;
    
    // variable pour contenir l'etat de chaque machine , si elle a recu un agent mobile ou non
    private boolean etat =false;
   
    public Intermediaire(){
        
        
        do {
            
            //  si la machine n'a pas encore recu un agent mobile elle va etre bloquee (tant que l'etat == false  )
            do {
                System.out.println("j'attends un agent mobile toujours");
            }
            while (!etat);
            // apres le passage de l'etat a true , le machine peut faire les traitements , choix du prochain voisin pour lui passer le paremetre ,etc
            /// toutes les methodes  doivent etre appeler dans le constructeur de chaque machine 
        } while (true);

    }

    public Compteur getCompteur() {
        return compteur;
    }

    /**
     * Return Agent Aiguilleur
     * @return
     */
    public Agent getAgent() {
        return this.aiguille;
    }

    public Agent getMobile() {
        return mobile;
    }

    public void setMobile(Agent mobile) {
        this.mobile = mobile;
    }
    // envoyer l'agent mobile à la machine passer en parametre 

    public void envoyerMobile(Intermediaire machine){
        machine.setMobile(mobile);
        // changer l'etat de la machine pour la notifer que l'agent mobile viens chez elle et elle peut dire à aiguilleur de faire le calcul necessaire pour passer l'agent à la machine elu 
        machine.setEtat(true);
        // ecraser le mobile de la machine en cours comme qu'elle viens de l'envoyer
        setMobile(null);
        //mettre lùetat de la machine  à 0 pour  en attendant qu'elle recoit un nouveau agent mobile
        setEtat(false);

    }
    // surchager la methode 
    public void envoyerMobile(Frodon machine){
        machine.setMobile(mobile);
        machine.setEtat(true);
        setMobile(null);
        setEtat(false);

    }




    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }
}

package Machine;

import java.util.ArrayList;

// Classe machine pour simuler un comportement d'une machine physique
public abstract class Machine {

    // Adresse de la machine
    private  String Adresse;
    // liste des machines voisins 
    ArrayList<Machine> Voisins = new ArrayList<Machine>();

    public Machine(){}
    public Machine(String ad){
        this.Adresse=ad;
    }
    public Machine(ArrayList<Machine> Voisins ){
        this.Voisins=Voisins;
    }
    public Machine(String ad,ArrayList<Machine> Voisins ){
        this.Adresse=ad;
        this.Voisins=Voisins;
    }
    
    // chaque machine elle doit enregister ses voisins  

    public void enregistrerVoisin(Machine machine){
        
        // Ajouter la machine passer en parametre en tant que voisin
        this.Voisins.add(machine);
    }
    // Une mchine peut ce supprimer de ses voisins pour une raison particuliere quand elle tombe en panne par exemple 
    public void supprimerVoisin(Machine machine){
        
        this.Voisins.remove(machine);
    }
    // Si la machine tombe en passe , elle parcouri ses voisins pour se supprimer en tant que machine voisin ( une coupure de reseau par exemple vers cette machine)
    public void enPanne(){
        for(Machine m : Voisins){
            m.supprimerVoisin(this);
        }
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }
   
    public ArrayList<Machine> getVoisins() {
        return Voisins;
    }

    public void setVoisins(ArrayList<Machine> voisins) {
        Voisins = voisins;
    }
}

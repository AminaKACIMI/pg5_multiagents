package AgentAiguilleur;

import Machine.*;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;


public class DemandePheromones extends Behaviour {
    private int step=0;
    private AgentAiguilleur agent;



    public DemandePheromones(AgentAiguilleur agent ){
        this.agent = agent;
    }


    @Override
    public void action() {
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        for (String m:this.agent.getListVoisin()) {


                msg.addReceiver(new AID(m));
                msg.setOntology("Weather-forecastontology");
                msg.setContent("true");
                myAgent.send(msg);


        }

    }


    @Override
    public boolean done() {
        return step==1;
    }
}
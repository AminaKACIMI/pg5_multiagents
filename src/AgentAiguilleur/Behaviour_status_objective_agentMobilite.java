package AgentAiguilleur;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class Behaviour_status_objective_agentMobilite extends OneShotBehaviour {
    /**
     * Variable
     */
    //setup agent
    private AgentAiguilleur agent;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_status_objective_agentMobilite(AgentAiguilleur agent){
        this.agent = agent;
    }

    /**
     * wait status of agentmobilite objectif's for TP
     */
    @Override
    public void action() {

        System.out.println("********************************************");
        System.out.println("aiguilleur : moove ");
        System.out.println("********************************************");

        //Wait response
        ACLMessage msg = myAgent.blockingReceive();
        if(msg != null){
            //Send response
            ACLMessage reply = msg.createReply();
            reply.setPerformative(ACLMessage.INFORM);
            reply.setContent("OK");
            myAgent.send(reply);

            //En fonction du premier message
            if(msg.getContent() == "objectif == true"){
                //On attend la deuxieme position
                System.out.println("L'agent mobile find objective !");
                ACLMessage msg_destination = myAgent.blockingReceive();
                String destination = "";

                if(msg_destination != null){
                    destination = msg_destination.getContent();
                }
                /**
                 * Code du TP choisie
                 * +
                 * Incremente
                 */
                //On informe de la teleportation
                ACLMessage msg_inform_tp= new ACLMessage(ACLMessage.INFORM);
                msg.addReceiver(new AID(destination,AID.ISLOCALNAME));
                msg.setLanguage("ENGLISH");
                msg.setOntology("Ontology_teleport");
                msg.setContent("TP inform !");

                ACLMessage reply_destination = myAgent.blockingReceive();
                if(reply_destination != null){
                    msg.createReply();
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent(reply_destination.getContent());
                    myAgent.send(reply);
                }
            }else{
                System.out.println("L'agent mobile not found objective");
                /**
                 * Code du tp random
                 * +
                 * Decremente
                 */
            }
        }
    }

    @Override
    public int onEnd() {
        return 0;
    }
}

package AgentAiguilleur;

import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class Behaviour_status_receiver extends OneShotBehaviour {
    /**
     * Variable
     */
    //Setup agent
    private AgentAiguilleur agent;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_status_receiver(AgentAiguilleur agent){
        this.agent = agent;
    }

    /**
     * Wait request status receiver and send response
     */
    @Override
    public void action() {

        System.out.println("********************************************");
        System.out.println("aiguilleur : status receiver  ");
        System.out.println("********************************************");

        ACLMessage msg = myAgent.blockingReceive();
        if(msg != null){
            if(msg.getContent() == "Receiver status"){
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.INFORM);

                reply.setContent(this.agent.getHaveReceiver() ? "true" : "false");

                myAgent.send(reply);
            }else{
                /**
                 * Demande de TP
                 */
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.INFORM);

                reply.setContent(this.agent.getNodeNumber()+"");

                myAgent.send(reply);
            }
        }
    }

    @Override
    public int onEnd() {
        return 0;
    }
}

package AgentAiguilleur;

import AgentAiguilleur.AgentAiguilleur;
import Machine.*;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.HashMap;

public class ReceptionPheromones extends Behaviour {
    private int step=0;

    private AgentAiguilleur agent;



    public ReceptionPheromones(AgentAiguilleur agent ){
        this.agent = agent;
    }






    public void action() {
        HashMap<String, Integer> TauxPh = new HashMap();
        for (Machine m:this.agent.getVoisins()) {

            ACLMessage msg = myAgent.receive();
            if(msg!=null){
                System.out.println("Message recu :"+msg.getContent());
                int taux= Integer.parseInt(msg.getContent());
                //je remplie la liste des nbre de pheromones des voisins
                if (taux>0){
                    TauxPh.put( m.getAdresse(), taux);
                }
                else {
                    System.out.println("Pherormones vides");
                }


            }
            else{
                block();
            }
        }
        this.agent.setTauxPh(TauxPh);
    }

    @Override
    public boolean done() {
        return false;
    }
}

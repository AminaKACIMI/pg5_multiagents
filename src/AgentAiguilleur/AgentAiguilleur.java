
// Package
///////////////
package AgentAiguilleur;

// Imports
///////////////
import jade.core.Agent;
import java.util.*;


import Machine.*;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;


public class AgentAiguilleur extends Agent
{

    //Liste des probabilites
    public HashMap<String, Integer> ListProba = new HashMap();
    //Listes des pheromones
    public HashMap<String, Integer> TauxPh = new HashMap();
    //liste voisins
    private ArrayList<Machine>Voisins = new ArrayList<>();

    //Nouvelle liste pour les voisins
    private ArrayList<String> listVoisin = new ArrayList<>();
    //boolean pour savoir si un receiver est sur la node
    private boolean haveReceiver = false;
    //Numero de la node
    private int nodeNumber = 0;

    //Somme des taux de pheromones
    private int sumph = 0;


    private final static String STATE_STATUT_RECEIVER = "statut_receiver";
    private final static String STATE_MOOVE = "statut_moove";

    /**
     * Methode lancee lors du demarrage de l'agent
     *
     */
    protected void setup() {
        Object[] args = getArguments();
        if(args == null){
            System.out.println("Manque arguments !");
            this.doDelete();
        }

        //Le premier args est le numero de la node
        this.nodeNumber = Integer.parseInt(args[0].toString());
        //Set receiver
        this.haveReceiver = args[1].toString() == "true" ? true : false;

        if(nodeNumber == 1 || nodeNumber == 2){
            System.out.println("Frodon => pas de voisin");
        }else {
            if(this.nodeNumber == 4 || this.nodeNumber == 5) {
                //Cas spécial pour les node 4 et 5 pour get les frodon 1 et 2
                this.listVoisin.add("compteur_" + 1);
                this.listVoisin.add("compteur_" + 2);
            }else {
                //Determine les deux voisins
                double voisinNumber = this.nodeNumber / 2;
                int firstVoisin = (int) Math.floor(voisinNumber);
                int secondVoisin = firstVoisin + 1;

                //Ajoute les voisins a la liste
                this.listVoisin.add("compteur_" + firstVoisin);
                this.listVoisin.add("compteur_" + secondVoisin);
            }
        }


        System.out.println("Hello  I’m gone show you the way ");

        //Je demande aux agents compteurs de me donner le nbre de pheromones

        FSMBehaviour fsm = new FSMBehaviour(this){
            @Override
            public int onEnd() {
                System.out.println("********************************************");
                System.out.println("FSM complete");
                System.out.println("********************************************");
                myAgent.doDelete();
                return super.onEnd();
            }
        };

        fsm.registerFirstState(new Behaviour_status_receiver(this), STATE_STATUT_RECEIVER);
        fsm.registerState(new Behaviour_status_objective_agentMobilite(this), STATE_MOOVE);
        fsm.registerLastState(new OneShotBehaviour() {
            @Override
            public void action() {
                System.out.println("Finish FSM");
            }
        }, "finsish");

        fsm.registerTransition(STATE_STATUT_RECEIVER, STATE_MOOVE, 0);
        fsm.registerTransition(STATE_MOOVE, STATE_STATUT_RECEIVER, 0);
        fsm.registerTransition(STATE_MOOVE, "finish", 1);

        addBehaviour(fsm);
    }



    /**
     *  renvoie la machine sur laquelle peut aller l'agent en utilisant l'algorithme fourmi
     * @return Object AgentAiguilleur.Machine
     */
    public String SelectHostRandomly (){
        CountProba();

        int choix= 0,i = 0,temp =0;

        int tab[] = new int[ListProba.size()+2];
        tab[0] = 0;
        tab[ListProba.size()+1] = sumph;

        String Machinevoisine = null;

        int propa = (int) (Math.random() *(sumph)); // selection d'une valeur aléatoire


        for ( int getpropa : ListProba.values()) {
            temp = temp + getpropa;
            tab[i+1] = temp;
            i++;
        }


        for (int j = 1; j <tab.length-1 ; j++) {
            if(propa>= tab[j-1] && propa<=tab[j+1]){
                choix = i;
            }
        }

        Iterator it = ListProba.entrySet().iterator();
        int compteur = 0;
        Map.Entry mapentry = null;

        while (it.hasNext() && compteur<=choix) {
            mapentry = (Map.Entry) it.next();
        }

        for (String m :listVoisin) {
            if (m.equals(mapentry)){
                Machinevoisine = m;
            }
        }
        return Machinevoisine;

    }

    public void CountProba(){

        // je parcours ma map en calculant la probabilite en suivant la formule fournis en doc
        for (Map.Entry mapentry : TauxPh.entrySet()) {

            //je transforme le message reçu en entier (le taux de pheromone)
            int proba = Integer.parseInt(mapentry.getValue().toString())/sumph;

            //j'enregistre dans une map la probabilité que chaque machine soit choisi
            ListProba.put(mapentry.getKey().toString(), proba);
        }

        sumph = somme();

    }

    /**
     * Permet de calculer la somme des taux de pheromones des voisins
     * @return
     */
    public int somme(){
        int sum = 0;
        for (Map.Entry mapentry : ListProba.entrySet()) {
            int ph= Integer.parseInt(mapentry.getValue().toString());
            sum = sum+ph;
        }
        return sum;
    }


    public HashMap<String, Integer> getListProba() {
        return ListProba;
    }

    public void setListProba(HashMap<String, Integer> listProba) {
        ListProba = listProba;
    }

    public HashMap<String, Integer> getTauxPh() {
        return TauxPh;
    }

    public void setTauxPh(HashMap<String, Integer> tauxPh) {
        TauxPh = tauxPh;
    }

    public ArrayList<String> getListVoisin() {
        return listVoisin;
    }

    public void setListVoisin(ArrayList<String> listVoisin) {
        this.listVoisin = listVoisin;
    }

    public ArrayList<Machine> getVoisins() {
        return Voisins;
    }

    public void setVoisins(ArrayList<Machine> voisins) {
        Voisins = voisins;
    }

    public boolean getHaveReceiver() { return this.haveReceiver; }

    public int getNodeNumber() {
        return nodeNumber;
    }
}

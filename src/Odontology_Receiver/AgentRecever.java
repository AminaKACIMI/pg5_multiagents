package Odontology_Receiver;

	
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import jade.core.behaviours.OntologyServer;
import jade.lang.acl.ACLMessage;

public class AgentRecever extends Agent {
	
public String contenu = "exemple" ; // ressource contenu par l'agent recerver
	
	
	protected void setup() {
		
		// Register language and ontology 
		getContentManager().registerLanguage(new SLCodec());
		getContentManager().registerOntology(TypeOntology.getInstance());
		
		addBehaviour(new OntologyServer(this, TypeOntology.getInstance(), ACLMessage.REQUEST, this));
	}
	
	public void serveSetTypeRequest(SetType st, ACLMessage request) {
		boolean r = ismathing(st); // le contenu du SetType st est comparé avec la varibale présente dans la classe pour savoir si ce qui a été demandé existe 
		ACLMessage reply = request.createReply();
		reply.setPerformative(ACLMessage.INFORM);
		reply.setContent(""+ r); // répond par true ou false selon que si la ressource existe ou pas 
		send(reply);
	}
	
	// compare entre deux chaines de caracteres
	public boolean ismathing(SetType wa) {
		
		if(contenu.equals(wa.getType())) {
			return true ;
		}
		return false ;
	}
	
	
	
}




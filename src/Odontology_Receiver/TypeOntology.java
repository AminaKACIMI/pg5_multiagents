package Odontology_Receiver;

import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;

public class TypeOntology extends BeanOntology {

	public static final String NAME = "Type-Ontology";

	
	// The singleton instance of the Type-Ontology
	private static TypeOntology theInstance = new TypeOntology();
	
	public static Ontology getInstance() {
		return theInstance;
	}
	
	public TypeOntology() {
		super(NAME);
		
		try {
			// Add all Concepts, Predicates and AgentActions in the local package
			add(getClass().getPackage().getName());
		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}

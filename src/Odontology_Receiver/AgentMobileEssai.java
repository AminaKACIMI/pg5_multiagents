package Odontology_Receiver;

import java.util.Date;

import jade.content.lang.sl.SLCodec;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.core.behaviours.Behaviour;

public class AgentMobileEssai extends Agent {
	
	public  String s = "exercice";
	public boolean objectif = false ;
	public AID ServerAgent; 
	public int step=0;
	
	protected void setup() {
		
		String timeServerAgentName = "server"; 
		Object[] args = getArguments();
		if (args != null && args.length > 0) {
			timeServerAgentName = (String) args[0];
			}
		ServerAgent = new AID(timeServerAgentName, AID.ISLOCALNAME);
		
		// Register language and ontology 
		getContentManager().registerLanguage(new SLCodec());
		getContentManager().registerOntology(TypeOntology.getInstance());
		
		// request type
		addBehaviour(new Behaviour(this) {
			
			public void action() {
				
				requestSetType(s);
				step++;
				
			}

			public boolean done() {	
				return step==1;
			}
		});
		
		
	}
	
	// cette méthode transmet au recever la variable s au qui contient la ressource recherché 
	private void requestSetType(String contain) {
		ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
		request.addReceiver(ServerAgent);
		request.setOntology(TypeOntology.getInstance().getName());
		request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
		try {
			SetType st = new SetType();
			st.setType(contain);
			Action actExpr = new Action(ServerAgent, st);
			getContentManager().fillContent(request, actExpr);
			addBehaviour(new AchieveREInitiator(this, request) {
				public void handleInform(ACLMessage inform) {
					if(inform.getContent().equals("true")) { // si la ressource a été trouvé 
						System.out.println("le receveur m'informe que la ressouce existe, j'ai trouvé ce que je cherchais");
						objectif = true ;
						System.out.println("mon objectif est à : "+objectif);
						
					} else { // si la ressource demandé n'existe pas dans le recever
						System.out.print("le receveur m'informe que la ressouce n'existe pas");
					}
					
				}
			});
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}

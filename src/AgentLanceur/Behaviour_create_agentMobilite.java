package AgentLanceur;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;

public class Behaviour_create_agentMobilite extends OneShotBehaviour {
    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
    private AgentLanceur agent;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_create_agentMobilite(AgentLanceur agent){
        this.agent = agent;
    }

    /**
     * Create agent mobilite
     */
    @Override
    public void action() {
        System.out.println("********************************************");
        System.out.println("Create Agents mobilite");
        System.out.println("********************************************");
        ArrayList<AgentController> listAgentMobilite = new ArrayList<>();

        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.MAIN_HOST, "localhost");
        profile.setParameter(Profile.GUI, "true");
        ContainerController containerController = runtime.createMainContainer(profile);

        for(int i = 0; i<this.agent.getNumberOfAgentMobilite(); i++){
            try {
                AgentController ac = containerController.createNewAgent("agentMobile_"+i,"AgentMobilite.AgentMobilite", null);
                //ac.start();
                listAgentMobilite.add(ac);
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }

        this.agent.setAgentsMobile(listAgentMobilite);
    }

    /**
     * Return 1 for skip to other state
     * @return
     */
    @Override
    public int onEnd() {
        return 1;
    }
}

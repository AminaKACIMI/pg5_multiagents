package AgentLanceur;

import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.wrapper.AgentController;

import java.util.ArrayList;

public class AgentLanceur extends Agent {

    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
    private ArrayList<AgentController> agentsMobile;
    private int numberOfAgentMobilite = 0;
    private Object objective;

    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Constantes
     * ------------------------------------------------------------------------------------------------------------------
     */
    private static final String STATE_CREATE_AGENT_MOBILE = "Create_agent_mobile";
    private static final String STATE_START_AGENT_MOBILE = "Start_agent_mobile";

    /**
     * Setup for agent lanceur
     * Arguments : number of agent mobilite start and objectif
     */
    @Override
    protected void setup() {
        //get arguments
        Object[] args = getArguments();

        /**
         * Check if not null
         */
        if(args.length == 0)
            System.out.println("Error not args !");

        /**
         * Set variable
         */
        this.agentsMobile = new ArrayList<>();
        this.numberOfAgentMobilite = Integer.parseInt(args[0].toString());
        this.objective = args[1].toString();

        /**
         * Check if not null
         */
        if(this.objective == null || this.numberOfAgentMobilite == 0){
            System.out.println("Stop process Agent lanceur -> args not define !");
            this.doDelete();
        }

        super.setup();

        /**
         * Create FSM behaviour
         */
        FSMBehaviour fsm = new FSMBehaviour(this){
            @Override
            public int onEnd() {
                System.out.println("********************************************");
                System.out.println("FSM complete");
                System.out.println("********************************************");
                myAgent.doDelete();
                return super.onEnd();
            }
        };

        /**
         * Set state
         */
        fsm.registerFirstState(new Behaviour_create_agentMobilite(this), STATE_CREATE_AGENT_MOBILE);
        fsm.registerLastState(new Behaviour_start_agentMobilite(this), STATE_START_AGENT_MOBILE);

        /**
         * Set transition
         */
        fsm.registerDefaultTransition(STATE_CREATE_AGENT_MOBILE, STATE_START_AGENT_MOBILE);

        /**
         * Start FSM
         */
        System.out.println("********************************************");
        System.out.println("Start FSM");
        System.out.println("********************************************");
        addBehaviour(fsm);
    }


    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Getter and setter
     * ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * Set AgentControleur for start agents mobile
     * @param agentsMobile
     */
    public void setAgentsMobile(ArrayList<AgentController> agentsMobile) {
        this.agentsMobile = agentsMobile;
    }

    /**
     * Get list of agentControlleur
     * @return
     */
    public ArrayList<AgentController> getAgentsMobile() {
        return agentsMobile;
    }

    /**
     * Get objectif of agent mobile
     * @return
     */
    public Object getObjective() {
        return objective;
    }

    /**
     * Set objectif of agent mobile
     * @return
     */
    public int getNumberOfAgentMobilite() {
        return numberOfAgentMobilite;
    }
}

package AgentLanceur;

import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.util.ArrayList;

public class Behaviour_start_agentMobilite extends OneShotBehaviour {
    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
    private AgentLanceur agent;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_start_agentMobilite(AgentLanceur agent){
        this.agent = agent;
    }

    /**
     * Run/Start agent mobilite
     */
    @Override
    public void action() {
        System.out.println("********************************************");
        System.out.println("Run agent mobilite");
        System.out.println("********************************************");
        ArrayList<AgentController> list = this.agent.getAgentsMobile();

        for(AgentController ac : list){
            try {
                System.out.println("Start agent mobile : "+ac.getName());
                ac.start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Return 1 for skip to other state
     * @return
     */
    @Override
    public int onEnd() {
        return 1;
    }
}

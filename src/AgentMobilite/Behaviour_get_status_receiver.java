package AgentMobilite;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class Behaviour_get_status_receiver extends OneShotBehaviour {
    /**
     * Variable
     */
    //Setup agent
    private AgentMobilite agent;
    private boolean receiverFound = false;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_get_status_receiver(AgentMobilite agent){
        this.agent = agent;
        this.receiverFound = this.agent.getStatus();
    }

    /**
     * Send msg for get status of agentReceiver
     */
    @Override
    public void action() {
        System.out.println("********************************************");
        System.out.println("vois si il y a un receiver ");
        System.out.println("********************************************");

        ACLMessage msg= new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(new AID("Aiguilleur_"+this.agent.getNodeNumber(),AID.ISLOCALNAME));
        msg.setLanguage("ENGLISH");
        msg.setOntology("Ontology_reciver_status");
        msg.setContent("Receiver status");
        myAgent.send(msg);

        ACLMessage reply = myAgent.blockingReceive();
        if(reply != null && this.agent.getStatus()){
            this.agent.setFoundReceiver(reply.getContent() == "true"? true : false);
            this.receiverFound = reply.getContent() == "true"? true : false;

            System.out.println("Found receiver ? "+receiverFound);
        }
    }

    @Override
    public int onEnd() {
        return receiverFound == true ? 1 : 0;
    }
}

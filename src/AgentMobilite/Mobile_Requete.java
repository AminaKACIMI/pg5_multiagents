package AgentMobilite;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class Mobile_Requete extends Behaviour{

	/**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
	private AgentMobilite agent;
	private int  step=0;

	public Mobile_Requete(AgentMobilite agent){
		super(agent);
		this.agent = agent;
	}
	/**
     * ------------------------------------------------------------------------------------------------------------------
     * -dans un premier temps on envoie la requete pour voire si la ressource est presente dans lagent 
     * -dans un deuxieme temps on recoit la reponse de  notre agent receiver
     * ------------------------------------------------------------------------------------------------------------------
     */
	
	public void action() {
		System.out.println("********************************************");
		System.out.println("Mobile request step : " + step);
		System.out.println("********************************************");
		switch (step){
			case 0 :
				ACLMessage msg= new ACLMessage(ACLMessage.INFORM);
				msg.addReceiver(new AID("AgentReceiver",AID.ISLOCALNAME));
				msg.setLanguage("ENGLISH");
				msg.setOntology("My ontology");
				msg.setContent(this.agent.getObjectif().toString());
				myAgent.send(msg);
				step++;
				break;
			case 1 :
				ACLMessage reply = myAgent.blockingReceive();
				System.out.println("\n*************Answer*************");
				if(reply != null){
					if (reply.getContent().equals("true")) {
						System.out.println("Find my objective");
						this.agent.foundOjectif();
					} else {
						System.out.println("Not find my objective");
					}
				}
				step ++;
				break;
			default: break;
		}
	}
	
	/**
     * ------------------------------------------------------------------------------------------------------------------
     * -return un boolean, qui est notre condition d'arret pour la methode action
     * ------------------------------------------------------------------------------------------------------------------
     */
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		if(step == 2){
			step = 0;
			return true;
		}
		return false;
	}

	@Override
	public int onEnd() {
		return 0;
	}
}

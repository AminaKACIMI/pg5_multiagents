package AgentMobilite;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

public class Behaviour_moove extends OneShotBehaviour {
    /**
     * Variable
     */
    //setup agent
    private AgentMobilite agent;

    /**
     * Constructor
     * @param agent
     */
    public Behaviour_moove(AgentMobilite agent){
        this.agent = agent;
    }

    /**
     * Send msg for moove
     */
    @Override
    public void action() {

        System.out.println("********************************************");
        System.out.println("moove ");
        System.out.println("********************************************");

        ACLMessage msg= new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(new AID("Aiguilleur_"+this.agent.getNodeNumber(),AID.ISLOCALNAME));
        msg.setLanguage("ENGLISH");
        msg.setOntology("Moove onthology");
        msg.setContent(this.agent.getStatus()? "objectif == true" : "objectif == false");
        myAgent.send(msg);

        ACLMessage reply = myAgent.blockingReceive();
        if(reply != null && this.agent.getStatus()){
            ACLMessage msg_destination = msg.createReply();
            reply.setPerformative(ACLMessage.INFORM);

            //On récupere le dernier
            msg_destination.setContent(this.agent.getStorePass().get(this.agent.getStorePass().size() - 1));
            //On enleve celui par lequel on viens de passer
            ArrayList<String> storePass = this.agent.getStorePass();
            storePass.remove(storePass.size() -1);
            this.agent.setStorePass(storePass);

            myAgent.send(msg_destination);

            ACLMessage reply_destination = myAgent.blockingReceive();
            if(reply_destination != null){
                String destination = "aiguilleur_"+reply_destination.getContent();
                int nodeNumber = Integer.parseInt(reply_destination.getContent());
                this.agent.setNodeNumber(nodeNumber);
                this.agent.addNodePass(destination);
                this.agent.doMoove(destination);

                System.out.println("Moove -> "+destination);
            }
        }
    }

    @Override
    public int onEnd() {
        return 0;
    }
}

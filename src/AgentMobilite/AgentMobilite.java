package AgentMobilite;

import jade.core.AID;
import jade.core.Agent;
import jade.core.PlatformID;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;

import java.util.ArrayList;

public class AgentMobilite extends Agent {
    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
    private ArrayList<String> storePass;
    private boolean haveFound = false;//default false //Found objectif
    private Object objectif;
    private int nodeNumber = 0; //default 0
    private boolean foundReceiver = false;

    /**
     * Constantes
     */
    private static final String STATE_GET_STATUS_RECEIVER = "state_get_status_receiver";
    private static final String STATE_CHECK_OBJECTIVE = "state_get_status_receiver";
    private static final String STATE_MOOVE = "state_moove";

    /**
     * Setup Agent
     */
    @Override
    protected void setup() {
        Object[] args = getArguments();
        //System.out.println("hello world i'm agent sender Mobile : je vous transmet ma requete");
        //objectif = "image ou texte";
        //this.objective = args[1].toString();

       if( args == null){
           this.objectif = "image ou texte";
       }else{
           this.objectif = args[0].toString();
       }

        //addBehaviour(new Mobile_Requete(this));

        FSMBehaviour fsm = new FSMBehaviour(this){
            @Override
            public int onEnd() {
                System.out.println("********************************************");
                System.out.println(" Agent Mobile -> FSM complete");
                System.out.println("********************************************");
                myAgent.doDelete();
                return super.onEnd();
            }
        };

        fsm.registerFirstState(new Behaviour_get_status_receiver(this), STATE_GET_STATUS_RECEIVER);
        fsm.registerState(new Mobile_Requete(this), STATE_CHECK_OBJECTIVE);
        fsm.registerState(new Behaviour_moove(this), STATE_MOOVE);
        fsm.registerLastState(new OneShotBehaviour() {
            @Override
            public void action() {
                System.out.println("Close FSM");
            }

            @Override
            public int onEnd() {
                return 1;
            }
        }, "finish");

        fsm.registerTransition(STATE_GET_STATUS_RECEIVER, STATE_MOOVE, 0);
        fsm.registerTransition(STATE_GET_STATUS_RECEIVER, STATE_CHECK_OBJECTIVE, 1);
        fsm.registerDefaultTransition(STATE_CHECK_OBJECTIVE, STATE_MOOVE);
        fsm.registerTransition(STATE_MOOVE, STATE_GET_STATUS_RECEIVER, 0);
        fsm.registerTransition(STATE_MOOVE, "finish", 1);

        System.out.println("********************************************");
        System.out.println("Agent Mobile -> Start FSM");
        System.out.println("********************************************");
        addBehaviour(fsm);
    }

    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Methods
     * ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * function addNodePass
     * Add to list storePass the node where it have passed
     * @param nodeIp
     */
    public void addNodePass(String nodeIp){
        this.storePass.add(nodeIp);
    }

    /**
     * Set value by true if it found objectif
     */
    public void foundOjectif(){
        this.haveFound = true;
    }

    public void doMoove(String destination){
        AID remoteAMS = new AID (destination,AID.ISLOCALNAME);
        PlatformID destination_aid = new PlatformID (remoteAMS);
        this.doMove (destination_aid);
    }

    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Getter and setter
     * ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * function GetStorePass
     * retrun list of nodes where it passed
     * @return
     */
    public ArrayList<String> getStorePass() {
        return storePass;
    }

    /**
     * function setStorePass
     * Change list of nodes where it passed by an other
     * @param storePass
     */
    public void setStorePass(ArrayList<String> storePass) {
        this.storePass = storePass;
    }

    /**
     * function getObjectif
     * returns the objective it is looking for
     * @return
     */
    public Object getObjectif() {
        return objectif;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public boolean getStatus(){
        return this.haveFound;
    }

    public void setFoundReceiver(boolean foundReceiver) {
        this.foundReceiver = foundReceiver;
    }

}

package AgentReceiver;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.core.behaviours.OneShotBehaviour;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

//import java.util.ArrayList;

public class Behaviour_create_agentReceiver extends OneShotBehaviour {
    /**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
   

    /**
     * Constructor
     * @param agent
     */
   

    /**
     * Create agent receiver
     */
    @Override
    public void action() {
        System.out.println("********************************************");
        System.out.println("Create Agents receiver");
        System.out.println("********************************************");
        

        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.MAIN_HOST, "localhost");
        profile.setParameter(Profile.GUI, "true");
        ContainerController containerController = runtime.createMainContainer(profile);

            try {
                AgentController ac = containerController.createNewAgent("mobile_receiver"+1,"AgentReceiver.AgentReceiver", null);
                ac.start();
               
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
       
    }

    /**
     * Return 1 for skip to other state
     * @return
     */
    @Override
    public int onEnd() {
        return 1;
    }
}
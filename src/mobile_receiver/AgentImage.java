package mobile_receiver;

import java.util.logging.Level;
import java.util.logging.Logger;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class AgentImage extends AgentReceveir {
	/**
     * ------------------------------------------------------------------------------------------------------------------
     * Variables
     * ------------------------------------------------------------------------------------------------------------------
     */
	
private String contenu = "image ou texte";
	
private transient Logger logger = Logger.getLogger(AgentReceveir.class.getName());
boolean isMatching=false; 

String queryRecu="";
	@Override
	
	
	/**
     * ------------------------------------------------------------------------------------------------------------------
     * -teste si la requte est bien recue, si oui on renvoit a (true or false) a l'agent mobile
     * -on associe un message positif on pas a notre boolean
     * ------------------------------------------------------------------------------------------------------------------
     */
	protected void setup() {
		System.out.println("\n*****************Hello I'm an agent Agent !************************\n");
		
		System.out.println("\n*****************My local Name is : "+getAID().getLocalName()+"\n");
		try {
	        addBehaviour(new CyclicBehaviour(this)
	        {
	             public void action() 
	             {
	                ACLMessage msg = myAgent.blockingReceive();
	                if (msg!=null) {
	                	System.out.println("I'm " + myAgent.getAID().getLocalName() + " et j'ai recu la requete: \n *****************Requete: " + msg.getContent());
						queryRecu = msg.getContent();
						System.out.println(" - " + myAgent.getLocalName() + " <- " + msg.getContent());
						GetType();

						//Trouve ou non son objectif
						boolean find = IsInformationMatching(msg.getContent());

						//Repond
						ACLMessage reply = msg.createReply();
						reply.setContent("" + find);
						reply.setPerformative(ACLMessage.INFORM);
						myAgent.send(reply);
						System.out.println("Send reply : " + find);
					}

	             }

	        });
	        Thread.sleep(500);
			}
			catch (Exception e) {
				logger.log(Level.INFO, "Got an exception.", e);
			}

	}
	
	@Override
	/**
     * function setType
     * definit le type de ressource
     * @return
     */
	
	protected void SetType(String s) {
		//this.contenu=s;
	}
	

	/**
     * ------------------------------------------------------------------------------------------------------------------
     * Return notre ressource
     * ------------------------------------------------------------------------------------------------------------------
     */

	@Override
	/**
     * function getType
     * recupere le type de ressource
     * @return
     */
	protected String GetType() {
		
		return contenu;
	}

	
	/**
     * ------------------------------------------------------------------------------------------------------------------
     * -si le contenu de la ressource match avec la requte on le dit sinon on informe quil ya en paas 
     * -et on repond a lagent mobile
     * ------------------------------------------------------------------------------------------------------------------
     */
	@Override
	/**
     * function IsInformationMatching
     * verifie verifie si la ressource match avec la requete
     * @return
     */
	protected boolean IsInformationMatching(String recu) {
		if(queryRecu.equals(contenu)) {
			System.out.println(" la requete existe bien dans ma source :");
			isMatching=true;
		}
		else {
			System.out.println("\nInfo Deloppeur:la requete existe pas dans la ressource demandee ********************\n");
		}
		return this.isMatching;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public void doDelete() {
	System.out.println("I'm dying");
	}


}

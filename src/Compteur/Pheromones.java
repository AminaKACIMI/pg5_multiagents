package Compteur;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class Pheromones extends Behaviour {
    //Appel à la classe Compteur
    Compteur compteur;
    private int steep=0;
//Compteutr doit être passé en paramètre pour que la classe puisse avoir accès au nombre de pheromone
    public Pheromones (Compteur c){
        this.compteur=c;
    }
    @Override
    public void action() {
        ACLMessage msg = myAgent.receive();
        
        if(msg!=null){
            String msgRecu = msg.getContent();
            // verification si l'information cherchée est trouvée donc on incrémente le nombre de pheromones
            if (msgRecu.equals("true")){
                compteur.incPhero();
                System.out.println("nbres pheromones apres incrementation:" + compteur.getPheromones());
                steep++;


            }
            //si l'information cherchée n'est pas encore trouvée donc on décrémente le nombre de pheromones 
            else{
                    compteur.decPhero();
                    System.out.println("nbres pheromones apres deccrementation :" + compteur.getPheromones());
                steep++;

            }
        



        }
        else{
            block();
        }
    }

    @Override
    public boolean done() {
        return steep==1;
    }
}

package Compteur;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class EnvoiePheromones extends Behaviour {
    private int steep=0;
    //appel à la classe Compteur pour qu'on puisse accèder à la variable pheromone
    Compteur comp ;
//L'AGENT doit etre passé en paramètre à l'appel du behavior
    public EnvoiePheromones(Compteur c){
        this.comp = c;
    }
    @Override
    public void action() {
        
        ACLMessage msg = myAgent.blockingReceive();
        if(msg!=null){
            msg.getContent() ;
            System.out.println("Reponse :");
            //Après la reception du message de  l'agent aiguilleur , on lui répond en envoyant le nombre de pheromone sous forme d'une chaine de caractères 
            ACLMessage reply = msg.createReply();
            reply.setPerformative( ACLMessage.INFORM );
            reply.setContent(Integer.toString(comp.getPheromones()));
            myAgent.send(reply);

        }
        else{
            block();
        }
    }
    @Override
    public boolean done() {
        return steep==1;
    }
}

package Compteur;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;

public class Compteur extends Agent {
    //initialisation de nombre de pheromone à 0
    private int pheromones = 0;
    
    protected void setup() {
        // Printout a welcome message
        System.out.println("Hello  I’m an agent Compteur ");
        //Envoie le nombre de pheromones après avoir reçu un message de l'agent aiguilleur demedant le nombre de pheromones 
        addBehaviour(new EnvoiePheromones(this));

        //addBehaviour(new Pheromones(this));


    }
    

    public int getPheromones() {
        return pheromones;
    }

    public void setPheromones(int pheromones) {
        this.pheromones = pheromones;
    }
    //Incrémentation du nombre de pheromones quand que l'agent trouve l'information cherchée et passe par une machine 
    public void incPhero(){
        pheromones++;
    }
    //Décrementation du nombre de pheromone à chaque fois que l'agent mobile passe par un machine et n'a pas trouvé l'information, si le nombre de pheromone est égal à zero donc il va garder le 0
    public void decPhero(){
        if(pheromones>0){
            pheromones--;
        }
    }

    public void envoiePheromones(){

    }

}

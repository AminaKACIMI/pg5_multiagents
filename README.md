# Projet Master 1 - Informatique répartie 

Ce projet est réalisé par les étudiants de  **Master 1 ICONE**. 
Réalisation d'un systéme multi-agents grace à JADE :

 - en simulant un reseau virtuel
 - contenat n machines
 -  2 serveurs: Frodon01 et Frodon02 contenant chacuns des sources
 - On considere que les autres machines ne peuventpas accéder directement à frondon01 et frodon02 mais doivent passer par des machines intermédiaires 
 - selon la régle suivante : frodonN peut accéder à frodonN2 et frodonN2+1.

Membres de l'équipe : 

 - KACIMI Amina 
 - METAYER Justin 
 - MBOUP Issa 
 - CHALGHAF Nahed 
 - Ndeye marie yague
 - AMIRAT Kenza
 - Mohamed HASSAN KHAIREH
 - MOHAMED AHMED Sidi Mohamed


# Environnement de travail 

 - Intellij IDE 
 - JADE

# Fichiers 
Nous avons structuré notre projet en créent les classes suivantes :

 - Aiguilleur : permet de montrer le chemin aux agents mobiles 
 - Compteur : déployés sur les machines et simulent le fonctionnement des phéromones.
 - Lanceur : Interface utilisateur, envoie les agents .
 - Receveur : lien entre agents mobiles et bdd.
 - Mobilite : permet de se déplacer entre les noeuds (serveurs) pour essayer de trouver son objectif

 




